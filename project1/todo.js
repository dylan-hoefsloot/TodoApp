$(document).ready(function(e) {

  var webserverURL = "https://limitless-anchorage-96177.herokuapp.com/";

  function processTaskFromDatabase(currentTodo){
          
    if(currentTodo == null){ return; }

    var taskName = currentTodo.item; 
    var userName = currentTodo.username; 
    var taskHTML = '<li><span class="done">%</span>'; 
    taskHTML += '<span class="edit">+</span>'; 
    taskHTML += '<span class="delete">x</span>'; 
    taskHTML += '<span class="id">' + currentTodo.id + '</span>';
    taskHTML += ': ';
    taskHTML += '<span class="task"></span>'; 
    taskHTML += " - ";
    taskHTML += '<span class="user"></span></li>'; 
    var $newTask = $(taskHTML); 
    $newTask.find('.task').text(taskName); 
    $newTask.find('.user').text(userName); 
    $newTask.hide(); 
    if(!currentTodo.completed) $('#todo-list').append($newTask);
    else $('#completed-list').append($newTask);
    $newTask.show('clip', 250).effect('highlight', 1000);
  }

  $.ajax({
    method: 'GET',
    url: webserverURL + 'api/todos/',
    success: (result) => {
      if(result.todos == null) return;
      for(var i = 0; i < result.todos.rows.length; i++){
        processTaskFromDatabase(result.todos.rows[i])
      }
    }
  })

  $('#add-todo').button({ icons: { primary: "ui-icon-circle-plus" } });
  
  $('#new-todo').dialog({ modal : true, autoOpen : false });

  $('#add-todo').button({ icons: { primary: "ui-icon-circle-plus" }}).click( function() { $('#new-todo').dialog('open'); }); 
  
  $('#new-todo').dialog({ modal : true, autoOpen : false, buttons : { 
    
    "Add task" : function () {

      if ($('#newtask').val() === "") {
        $(this).dialog('close');
        return false; 
      }

      $.ajax({
        method: 'PUT',
        url: webserverURL + 'api/todos/',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: JSON.stringify({
          "item": $('#newtask').val(),
          'username':  $('#newuser').val()
        }),
        success: (result) => { 
          if(result.todo == null) return;
          processTaskFromDatabase(result.todo) 
        }
      })  

      $(this).dialog('close');

    }, 

    "Cancel" : function () { 
      $(this).dialog('close');  
    }
    
    },

    "open" : function() { 
      originalContent = $("#new-todo").html();
    },
    
    "close" : function() {
      $("#new-todo").html(originalContent);
    }

  }); 

  $('#todo-list').on('click', '.done', function() { 

    var $taskItem = $(this).parent('li');
    
    var taskName = $taskItem.find('.task').text(); 
    var userName = $taskItem.find('.user').text();

    $.ajax({
      method: 'POST',
      url: webserverURL + 'api/todo/' + $taskItem.find('.id').text(),
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: JSON.stringify({
        "item": taskName,
        'username': userName,
        'completed': true
      }),
      success: (result) => {
        $taskItem.slideUp(250, function() { 
          var $this = $(this); 
          $this.detach(); 
          $('#completed-list').append($this); 
          $this.slideDown(); 
        });  
      }
    })

  });

  $('.sortlist').sortable({ 
    connectWith : '.sortlist', 
    cursor : 'pointer', 
    placeholder : 'ui-state-highlight',
    cancel : '.delete, .edit, .done' 
    //display: none
  });
  
  $('#deletedialog').dialog({
    autoOpen: false,
    modal: true
  });

  $('.sortlist').on('click', '.delete', function(e) { 

    var currentItem = this;

    $("#deletedialog").dialog({ modal : true, autoOpen : false, buttons: {
        
        "Confirm" : function() {
          
          $.ajax({
            method: 'DELETE',
            url: webserverURL + 'api/todo/' + $(currentItem).parent('li').find('.id').text(),
            success: (result) => {
              $(currentItem).parent('li').effect('puff', function() {
                $(currentItem).remove();
              });
            }
          })

          $(this).dialog("close");
        },

        "Cancel" : function() {
          $(this).dialog("close");
        }

      }
    });

    $("#deletedialog").dialog("open");

  }); 

  $('#editdialog').dialog({
    autoOpen: false,
    modal: true
  });
  
  $('#todo-list').on('click', '.edit', function() { 

    var currentItem = $(this).parent('li');

    $("#editdialog").dialog({modal : true, autoOpen : false, buttons: {
          
        "Confirm" : function() {

          var taskName = $('#task').val(); 
          var userName = $('#user').val(); 

          if (taskName === "" || userName === "") { return false; }
          
          $.ajax({
            method: 'POST',
            url: webserverURL + 'api/todo/' + $(currentItem).find('.id').text(),
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: JSON.stringify({
              "item": taskName,
              'username': userName,
              'completed': false
            }),
            success: (result) => {
              currentItem.find('.task').text(taskName);       
              currentItem.find('.user').text(userName);    
            }
          })

          $(this).dialog('close'); 
        },

        "Cancel" : function() {
          $(this).dialog("close");
        },
      },
      
      "open" : function() { 
        $("#editdialog").find('#task').val(currentItem.find('.task').text());
        $("#editdialog").find('#user').val(currentItem.find('.user').text());
      }

    });

    $("#editdialog").dialog("open");

  });

}); // End ready
