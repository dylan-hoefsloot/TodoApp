const PORT = process.env.PORT || 8080;
const express = require('express');
const app = express();

const bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const { Pool } = require('pg');
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: true
});

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*')
  
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
  // Request headers you wish to allow 
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
  
  // Pass to next layer of middleware
  next();

});

app.use('/', express.static(__dirname + '/project1'));

app.get('/api/todos', async(req, res) => {
  
  try {
      
    const client = await pool.connect()
    const databaseInformation = await client.query('SELECT * FROM Todos');
      
    if(!databaseInformation){
      res.status(400).send({
        success: 'false',
        message: 'There was no data present in the database'
      })
    } 
    else {
      res.status(201).send({
        success: 'true',
        message: 'Todos retrieved successfully',
        todos: databaseInformation
      })
    } 
    
    client.release();

  } catch (err) { 
    
    console.error(err);
    res.status(400).send({
      success: 'false',
      message: 'An error has occured while fetching the todos',
    })
  }
  
});

app.put('/api/todos', async(req, res) => {

  if(!req.body.item) {
    return res.status(400).send({
      success: 'false',
      message: 'Item field is required'
    });
  }

  if(!req.body.username) {
    return res.status(400).send({
      success: 'false',
      message: 'User field is required'
    });
  }

  try {
      
    const client = await pool.connect()
    
    const databaseResponse = await client.query('SELECT MAX(id) FROM Todos');

    var maxIndex = databaseResponse.rows[0].max;

    if(maxIndex == null) maxIndex = 0;

    const newResponse = await client.query('INSERT INTO Todos VALUES (' + (maxIndex + 1) + ', \'' + req.body.item + '\', \'' + req.body.username + '\', false);');
    
    if(newResponse == null){
      console.error(err);
      res.status(400).send({
        success: 'false',
        message: 'An error has occured trying to add to the database',
      })
    }

    res.status(201).send({
      success: 'true',
      message: 'Todo added successfully',
      todo: {
        id: (maxIndex + 1),
        item: req.body.item,
        username: req.body.username
      }
    })

    client.release();

  } catch (err) { 
    
    console.error(err);
    res.status(400).send({
      success: 'false',
      message: 'An error has occured trying to add a todo',
    })

  }

});

app.post('/api/todo/:id', async(req, res) => {

  if(!req.body.item) {
    return res.status(400).send({
      success: 'false',
      message: 'Item field is required'
    });
  }

  if(!req.body.username) {
    return res.status(400).send({
      success: 'false',
      message: 'User field is required'
    });
  }

  if(!req.body.completed) {
    return res.status(400).send({
      success: 'false',
      message: 'Completed field is required'
    });
  }

  const itemID = parseInt(req.params.id, 10);

  try {
      
    const client = await pool.connect()
    
    const newResponse = await client.query('UPDATE Todos SET item = \'' + req.body.item + '\', username = \'' + req.body.username + '\', completed = ' + req.body.completed + ' WHERE id =' + itemID + ';');
    
    if(newResponse == null){
      console.error(err);
      res.status(400).send({
        success: 'false',
        message: 'An error has occured trying to edit the database',
      })
    }

    client.release();

    return res.status(201).send({
      success: 'true',
      message: 'Todo edited successfully'
    })

  } catch (err) { 
    
    console.error(err);

    res.status(400).send({
      success: 'false',
      message: 'An error has occured trying to edit a todo',
    })

  }

});


app.delete('/api/todo/:id', async(req, res) => {

  const itemID = parseInt(req.params.id, 10);

  try {
      
    const client = await pool.connect();

    const newResponse = await client.query('DELETE FROM Todos WHERE id = ' + itemID + ';');
    
    if(newResponse == null){
      console.error(err);
      res.status(400).send({
        success: 'false',
        message: 'An error has occured trying to delete from the database',
      })
    }

    client.release();

    return res.status(201).send({
      success: 'true',
      message: 'Todo deleted successfully'
    })

  } catch (err) { 
    
    console.error(err);
    res.status(400).send({
      success: 'false',
      message: 'An error occured trying to delete a todo',
    })

  }

})

app.listen(PORT, () => {
  console.log(`Listening on ${ PORT }`);
});
