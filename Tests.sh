#!/bin/sh

clear

echo "Adding todo"
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -H "Content-Type: application/json" -X PUT -d '{"item":"Test Task", "username":"Test User"}' https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
sleep 1
scrot
clear

echo "Editing todo"
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -H "Content-Type: application/json" -X POST -d '{"item":"Edited Task", "username":"Edited User", "completed":"false"}' https://limitless-anchorage-96177.herokuapp.com/api/todo/1
echo
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
sleep 1
scrot
clear

echo "Completing todo"
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -H "Content-Type: application/json" -X POST -d '{"item":"Completed Task", "username":"Usere", "completed":"true"}' https://limitless-anchorage-96177.herokuapp.com/api/todo/1
echo
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
sleep 1
scrot
clear

echo "Deleting todo"
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -X DELETE https://limitless-anchorage-96177.herokuapp.com/api/todo/1
echo
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
sleep 1
scrot
clear

echo "Testing error checking"
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -H "Content-Type: application/json" -X PUT -d '{"item":"Test Task"}' https://limitless-anchorage-96177.herokuapp.com/api/todos
echo
curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
sleep 1
scrot
clear

#time curl -X GET https://limitless-anchorage-96177.herokuapp.com/api/todos
#time curl -H "Content-Type: application/json" -X PUT -d '{"item":"Test Task", "username":"Test User"}' https://limitless-anchorage-96177.herokuapp.com/api/todos
#time curl -H "Content-Type: application/json" -X POST -d '{"item":"Completed Task", "username":"User", "completed":"true"}' https://limitless-anchorage-96177.herokuapp.com/api/todo/1
#time curl -X DELETE https://limitless-anchorage-96177.herokuapp.com/api/todo/1
